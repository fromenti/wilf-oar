#ifndef TRIM_HPP
#define TRIM_HPP

static const size_t k=3;

inline bool
trim_wilf1(Semigroup& S){

  return k*S.e_left>=S.m;
}

inline bool
trim_wilf2(Semigroup& S){
  return (k*S.e)>=S.m+k*(g_max-S.g);
}

inline bool
trim(Semigroup& S){
  return S.m==S.c;// or trim_wilf1(S) or trim_wilf2(S);
}

#endif
