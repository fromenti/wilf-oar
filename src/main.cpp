#include <sstream>
#include <fstream>
#include <list>
#include <deque>
#include <cmath>
#include <vector>
#include "walk.hpp"
struct{
  bool operator()(const Semigroup& l, const Semigroup& r) const { return l.cost > r.cost; }
} customLess;

void compute_cost(Semigroup& S){
  if(S.g>=g_max-10) S.cost=0;
  else{
    Stack stack;
    stack.next()->copy(S);
    stack.push();
    Results results;
    results.clear();
    walk(stack,results,min(g_max,S.g+4));
    double r=pow((double)results.n,0.25);
    S.cost=pow(r,log(double(g_max-S.g)));
  }
}

bool must_split(const Semigroup& S){
  return (S.g<g_max) and ((S.left<4 and S.cost>=min_cost_to_split and true) or (S.left==1));
}

void split(const Semigroup& S,deque<Semigroup>& to_explore){
  list<Semigroup> res; 
  auto it=SonIterator(&S);
  size_t irr_pos=0;
  while(it.move_next()){
    Semigroup& son=to_explore.emplace_back();
    son.son(S,it.get_gen(),irr_pos++);
  }
}

void set_tasks(vector<Semigroup>& tasks){
  deque<Semigroup> to_explore;
  Semigroup& root=to_explore.emplace_back();
  root.init_N();
  while(not to_explore.empty()){
    Semigroup S=to_explore.front();
    compute_cost(S);
    //cout<<S.cost<<endl;
    to_explore.pop_front();
    if(must_split(S)){
      split(S,to_explore);
    }
    else{
      tasks.push_back(S);
    }       
  }
}

using namespace std;
int main(){
  cout<<"g_max = "<<g_max<<endl;

  vector<Semigroup> tasks;
  set_tasks(tasks);
  size_t nb_tasks=tasks.size();
  cout<<"Number of tasks : "<<nb_tasks<<endl;

  
  Results results;
  results.clear();
  Stack stack;
  size_t temp=0;
  for(size_t i=0;i<nb_tasks;++i){
    results.clear();
    Semigroup& S=tasks[i];
    treat(S,results);
    //stack.next()->copy(S);
    //stack.push();
    //walk(stack,results);
    if(S.cost>=10) ++temp;
    /*if(S.g < g_max-10 and results.n>=10){
      cout<<S.cost<<" "<<results.n<<endl;
      }*/
  }
  cout<<temp<<endl;

  /*size_t m_max=0;
  for(int m=0;m<=g_max;++m){
    cout<<m<<" "<<results.n2[m]<<endl;
    }*/

  
			  
}
