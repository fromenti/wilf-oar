#include "walk.hpp"

void walk(Stack& stack,Results &results,string filename,int timeout){
  const auto start = time(nullptr);
  size_t numbers=0;
  Semigroup* father=new Semigroup;
  while(not stack.is_empty()){
    ++numbers;
    if(numbers%10000000==0){
      const auto now = time(nullptr);
      auto duration=now-start;
      if(duration>timeout){
	cout<<"Timeout"<<endl;
	cout<<"Stack size : "<<stack.size<<endl;
	if(timeout>60){
	  for(size_t i=0;i<stack.size;++i){
	    cout<<i<<" : ";
	    string filename2=filename+"_"+to_string(i);
	    cout<<filename2<<endl;
	    stack.data[i]->to_file("todo/"+filename2);
	  }
	  results.to_file("output/"+filename);
	  if(results.has_counter_example){
	    results.S_counter_example.to_file("counter_example/"+filename);
	  }
	  rename(("lock/"+filename).c_str(),("done/"+filename).c_str());
	}
	else{
	  rename(("lock/"+filename).c_str(),("todo/"+filename).c_str());
	}
	return;
      }
    }
    stack.pop(&father);
    auto it=SonIterator(father);
    size_t irr_pos=0;
    size_t g=father->g;
    if(g<g_max-1){
      while(it.move_next()){
	Semigroup* son=stack.next();
	son->son(*father,it.get_gen(),irr_pos++);
	if(not trim(*son)){
	  stack.push();
	  treat(*son,results);
	}
      }
    }
    else{
      while(it.move_next()){
	Semigroup* son=stack.next();
	son->son(*father,it.get_gen(),irr_pos++);
	if(not trim(*son)){
	  treat(*son,results);
	}
	
      }
    }     
  }
  delete father;
}
