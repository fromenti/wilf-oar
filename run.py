import os
import sys

def make(gmax):
    os.chdir("src")
    os.system("make clean > /dev/null")
    os.system("make GMAX="+repr(gmax)+" > /dev/null")
    os.chdir("../")

def init():
    os.system("./init_files > /dev/null")
    todo=os.listdir("todo")
    print("There is",len(todo),"files to treat");

def filter():
    todo=os.listdir("todo")
    print("=> Filter <=")
    print("in :",len(todo))
    todo_flat=' '.join(todo)
    cmd="parallel ./run_walk ::: "+todo_flat+" ::: 1 > /dev/null"
    os.system(cmd)
    todo2=os.listdir("todo")
    rem=set(todo).intersection(todo2)
    print("rem :",len(rem))
    print("done :",len(todo)-len(rem))
    print("new :",len(todo2)-len(rem))

def run():
    
if __name__=="__main__":
    if(len(sys.argv)!=2):
        print("Argument error")
        exit(-1)
    gmax=int(sys.argv[1])
    print("gmax =",gmax)
    make(gmax)
    init()
    filter()

    #run(timeout)
    
    
    
    
