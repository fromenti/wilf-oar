#ifndef TREAT_HPP
#define TREAT_HPP

#include "results.hpp"

void treat(const Semigroup& S,Results &results);

inline
void treat(const Semigroup& S,Results &results){
  assert(S.g<=g_max);
  results.ng[S.g]++;
}


#endif
