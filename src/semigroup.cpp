#include "semigroup.hpp"

void
Semigroup::init_N(){
  c=1;
  g=0;
  m=1;
  e=1;
  e_left=0;
  left=1;
  for(size_t x=0;x<size;++x){
    delta[x]=1+x/2;
  }
  for(size_t x=0;x<nin;++x){
    in64[x]=18446744073709551615UL;
  }
}

void
Semigroup::son(const Semigroup& S,size_t  x,size_t pos){
  c=x+1;
  g=S.g+1;
  int e_delta;
  if(x>S.m){
    m=S.m;
    e_delta=(S.delta[x+S.m]==2)?0:-1;
  }
  else{
    m=S.m+1;
    e_delta=1;
  }
  e=S.e+e_delta;
  e_left=S.e_left+pos;
  left=S.left+x-S.c;
  copy_delta(S);
  copy_in(S);

  size_t sx=x%32;
  size_t bz=0;
  v8x8 z;
  z.v64=0;
  epu64 w;
 
  for(size_t bx=x/32;bx<navx;++bx){
    uint64_t temp=in32[bz];
    z.v64+=(temp<<sx);
    w.data[0]=load_mask[z.v8[0]];
    w.data[1]=load_mask[z.v8[1]];
    w.data[2]=load_mask[z.v8[2]];
    w.data[3]=load_mask[z.v8[3]];
    
    avx[bx]=_mm256_sub_epi8(avx[bx],w.avx);
    z.v32[0]=z.v32[1];
    z.v32[1]=0;
    ++bz;       
  }

  in64[x/64]^=(1UL<<(x%64));  
}

void
Semigroup::display() const{
  bool flag=false;
  cout<<'<';
  for(size_t x=1;x<size;++x){
    if(delta[x]==1){
      if(flag) cout<<',';
      flag=true;
      cout<<x;
    }
  }
  cout<<"> m = "<<m<<" g = "<<g<<" c = "<<c<<endl;;
  /*for(size_t x=0;x<80;++x){
    if(x<10) cout<<' ';
    cout<<(int)x<<'|';
  }
  cout<<endl;
  for(size_t x=0;x<80;++x){
    cout<<"--|";
  }
  cout<<endl;
  for(size_t x=0;x<80;++x){
    if(delta[x]<10) cout<<' ';
    cout<<(int)delta[x]<<'|';
  }
  cout<<endl;
  for(uint64_t x=0;x<80;++x){
    //    cout<<x<<" "<<x/64<<" and "<<(1UL<<(x%64))<<endl;
    if((in64[x/64U]&(1UL<<(x%(64))))==0){
      cout<<" n|";
    }
    else{
      cout<<" y|";
    }
  }
  cout<<endl;*/
  
}

void
Semigroup::copy(const Semigroup& S){
  c=S.c;
  m=S.m;
  g=S.g;
  e=S.e;
  e_left=S.e_left;
  left=S.left;
  copy_delta(S);
  copy_in(S);
  
}

void
Semigroup::to_file(string filename) const{
  fstream file;
  file.open(filename.c_str(),ios::out|ios::binary|ios::trunc);
  file.write((char*)delta,size);
  file.write((char*)in64,nin*8);
  file.write((char*)&c,sizeof(size_t));
  file.write((char*)&m,sizeof(size_t));
  file.write((char*)&g,sizeof(size_t));
  file.write((char*)&e,sizeof(size_t));
  file.write((char*)&e_left,sizeof(size_t));
  file.write((char*)&left,sizeof(size_t));
  file.close();
}
			 
void
Semigroup::from_file(string filename){
  fstream file;
  file.open(filename.c_str(),ios::in|ios::binary);
  file.read((char*)delta,size);
  file.read((char*)in64,nin*8);
  file.read((char*)&c,sizeof(size_t));
  file.read((char*)&m,sizeof(size_t));
  file.read((char*)&g,sizeof(size_t));
  file.read((char*)&e,sizeof(size_t));
  file.read((char*)&e_left,sizeof(size_t));
  file.read((char*)&left,sizeof(size_t));
  file.close();
}
			 
