
#include "walk.hpp"

int main(int argc,char* argv[]){
  if(argc!=3){
    cerr<<"[run_walk] Argument error."<<endl;
    exit(-1);
  }
  string filename=argv[1];
  int timeout=stoi(argv[2]);
  cout<<"Input filename : "<<filename<<endl;
  cout<<"Timeout : "<<timeout<<" seconds"<<endl;
  Stack stack;
  Semigroup* S=stack.next();
  rename(("todo/"+filename).c_str(),("lock/"+filename).c_str());
  S->from_file("lock/"+filename);
  stack.push();
  Results results;
  results.clear();
  walk(stack,results,filename,timeout);
  if(results.has_counter_example){
    cout<<"*******************"<<endl;
    cout<<"* Counter example *"<<endl;
    cout<<"*******************"<<endl;
    results.S_counter_example.display();
  }
}
