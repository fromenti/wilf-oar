#ifndef WALK_HPP
#define WALK_HPP

#include <time.h>
#include "stack.hpp"
#include "trim.hpp"
#include "treat.hpp"
#include "results.hpp"

void walk(Stack& stack,Results &results,string filename,int timeout);

#endif
