#include "stack.hpp"

Stack::Stack(){
  data=new Semigroup*[capacity];
  for(size_t i=0;i<capacity;++i){
    data[i]=new Semigroup;
  }
  size=0;
}


Stack::~Stack(){
  for(size_t i=0;i<capacity;++i){
    delete data[i];
  }
  delete[] data;
}

void
Stack::display(){
  cout<<"---- Stack -----"<<endl;
  for(size_t i=0;i<size;++i){
    cout<<i<<"\t"<<data[i]<<endl;
  }
  cout<<"next \t"<<data[size]<<endl;
  cout<<"----------------"<<endl;
}
